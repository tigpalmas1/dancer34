import {
    CLEAR_FOUND_ITENS_LIST,
    DATE_UPDATE,
    ESTABLISHMENT_LOADING, ESTABLISHMENT_SAVE_ERROR, ESTABLISHMENT_SAVE_SUCCESS,

    ESTABLISHMENT_UPDATE, FETCH_CATEGORY_ERROR, FETCH_CATEGORY, FETCH_CATEGORY_LOADING, FETCH_ESTABLISHMENT_TICKETS,
    FETCH_FAVORIT_TICKETS,
    FETCH_FAVORIT_TICKETS_ERROR,
    FETCH_FAVORIT_TICKETS_LOADING, FETCH_PRODUCTS, FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_LOADING,
    FETCH_STAMPS, FETCH_TICKET_LOADING,
    FETCH_TICKETS, FETCH_TICKETS_ERROR, IMAGE_UPDATE,
    LOGIN_SUCCESS, TICKET_LOADING,
    TICKET_SAVE_ERROR,
    TICKET_UPDATE, UPDATER_USER_LOCATION, CATEGORY_UPDATE, FETCH_CATEGORY_PRODUCTS,

} from "./types";
import {getStorage, setStorage} from "../../components/common/functions/index";
import Parse from 'parse/react-native';
import {Location, Permissions, IntentLauncherAndroid} from 'expo';
import _ from 'lodash';



export const categoryUpdate = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: CATEGORY_UPDATE,
        payload: {prop, value}
    })
};



export const fetch_only_category = () => async (dispatch) => {

    try {

        const Category = Parse.Object.extend("Category");
        const queryCategories = new Parse.Query(Category);
        const resultCategories = await queryCategories.find();

        const categoriesJson = await _.map(resultCategories, function (item) {
            return item.toJSON()
        })


        dispatch({
            type: FETCH_CATEGORY,
            payload: {
                categories: categoriesJson,
                message: categoriesJson.length=== 0 ? 'Nenhuma Categoria Encontrada' : ''
            }
        })
    }
    catch
        (e) {
        console.log(e.message)
        dispatch({
            type: FETCH_PRODUCTS_ERROR,
            payload: 'Ops, algo deu errado, tente novamente ' + e.message
        })
    }
}




export const fetch_category_products = () => async (dispatch) => {

    try {

        const Category = Parse.Object.extend("Category");
        const queryCategories = new Parse.Query(Category);
        const resultCategories = await queryCategories.find();

        const categoriesJson = await _.map(resultCategories, function (item) {
            return item.toJSON()
        })



        const Products = Parse.Object.extend("Product");
        const queryProducts = new Parse.Query(Products);
        queryProducts.equalTo('active', true)
        queryProducts.include('categoryId')
        const resultsProducts = await queryProducts.find();

        const productsJson = await _.map(resultsProducts, function (item) {
            return item.toJSON()
        })

        for (i = 0; i < categoriesJson.length; i++) {
            categoriesJson[i] =   _.assign({}, categoriesJson[i], { products: [] })
            for (j = 0; j < productsJson.length; j++) {
                if(productsJson[j].categoryId.objectId === categoriesJson[i].objectId){
                    categoriesJson[i].products.push(productsJson[j])
                }
            }
        }


       /* const result = _(resultsJson)
            .groupBy('categoryId.objectId')
            .map(group => ({
                categoryId: _.head(group).categoryId,
                products: _.map(group, o => _.omit(o, 'categoryId'))
            }))
            .value()*/

        categoriesJson.push({objectId: '+1', name: 'Adicionar Categoria'})

        console.log(categoriesJson)

        dispatch({
            type: FETCH_CATEGORY_PRODUCTS,
            payload: {
                categories_products: categoriesJson,
                message: categoriesJson.length=== 0 ? 'Nenhuma Categoria Encontrada' : ''
            }
        })
    }
    catch
        (e) {
        console.log(e.message)
        dispatch({
            type: FETCH_PRODUCTS_ERROR,
            payload: 'Ops, algo deu errado, tente novamente ' + e.message
        })
    }
}

export const addCategoryAction = (name, callback) => async (dispatch) => {
    console.log('entrando aqui')

    try {
        const CategoryClass = Parse.Object.extend('Category');
        let categoryObject = new CategoryClass();
        categoryObject.set('name', name)
        await categoryObject.save()


        const Category = Parse.Object.extend("Category");
        const queryCategories = new Parse.Query(Category);
        const resultCategories = await queryCategories.find();

        const categoriesJson = await _.map(resultCategories, function (item) {
            return item.toJSON()
        })

        const Products = Parse.Object.extend("Product");
        const queryProducts = new Parse.Query(Products);
        queryProducts.equalTo('active', true)
        queryProducts.include('categoryId')
        const resultsProducts = await queryProducts.find();

        const productsJson = await _.map(resultsProducts, function (item) {
            return item.toJSON()
        })

        for (i = 0; i < categoriesJson.length; i++) {
            categoriesJson[i] =   _.assign({}, categoriesJson[i], { products: [] })
            for (j = 0; j < productsJson.length; j++) {
                if(productsJson[j].categoryId.objectId === categoriesJson[i].objectId){
                    categoriesJson[i].products.push(productsJson[j])
                }
            }
        }

        categoriesJson.push({objectId: '+1', name: 'Adicionar Categoria'})

        dispatch({
            type: FETCH_CATEGORY,
            payload: {
                categories: categoriesJson,
                message: categoriesJson.length=== 0 ? 'Nenhuma Categoria Encontrada' : ''
            }
        })

        callback('Categoria salva com sucesso')
    }
    catch
        (e) {
        console.log(e.message)
        callback('ops, algo deu errado'+e.message)
    }
}



export const deleteCategoryAction = ( categoryId, callback) => async (dispatch) => {
    console.log(categoryId)

    try {
        const Category = Parse.Object.extend("Category");
        let categoryObject = new Category();

        categoryObject.id = categoryId.objectId;


        await categoryObject.destroy()

        callback('categoria  deletada com sucesso')

    }
    catch(e){
        callback('algo deu errado'+e.message)
    }
}







