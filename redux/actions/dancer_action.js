import {
    ADD_USER, ADD_USER_LOADING, AUTH_UPDATE, CLEAR_USER, DANCER_INFO_UPDATE, FETCH_USERS, FETCH_USERS_LOADING,
    IMAGE_DANCER_UPDATE,
    IMAGE_USER_UPDATE,
    SET_UPDATE_USER, UPDATE_USER,
    USER_INFO_UPDATE
} from "./types";

import Parse from 'parse/react-native';


export const dancerInfoUpdateAction = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: DANCER_INFO_UPDATE,
        payload: {prop, value}
    })
};
















