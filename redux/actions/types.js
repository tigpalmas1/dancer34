


//ADMINISTRATION
export const FETCH_STYLE = 'fetch_styles';
export const FETCH_CITY = 'fetch_cities';


//DANCER
export const IMAGE_DANCER_UPDATE = 'image_dancer_update'
export const DANCER_INFO_UPDATE = 'dancer_info_update'


//EVENTS
export const FETCH_EVENTS = 'fetch_events';
export const FETCH_EVENTS_LOADING = 'fech_events_loading';



//AUTH
export const AUTH_LOADING = 'auth_loading';
export const AUTH_UPDATE = 'auth_update';
export const IMAGE_USER_UPDATE = 'image_user_update';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_ERROR = 'login_error';
export const LOGIN_LOADING = 'login_loading';
export const SIGNUP_SUCCESS = 'signup_success';
export const SIGNUP_ERROR = 'signup_error';
export const SIGNUP_LOADING = 'signup_loading';



//USER INFO
export const USER_INFO_UPDATE = 'user_info_update';
export const SET_UPDATE_USER = 'set_update_user';
export const CLEAR_USER = 'clear_user';
export const FETCH_USERS = 'fetch_users';
export const FETCH_USERS_LOADING = 'fetch_users_loading';
export const ADD_USER = 'add_user';
export const ADD_USER_LOADING = 'add_user_loading';
export const UPDATE_USER = 'update_user';
export const UPDATER_USER_LOCATION = 'user_location'






//ESTABLISHMENT
export const FETCH_ESTABLISHMENT = 'fetch_establishment';
export const ESTABLISHMENT_UPDATE = 'establishment_update';
export const ESTABLISHMENT_IMAGE_UPDATE = 'estalbihsment_image_update';
export const ESTABLISHMENT_LOADING = 'establishment_loading';
export const ESTABLISHMENT_SAVE_SUCCESS= 'establishment_save_sucess';
export const ESTABLISHMENT_SAVE_ERROR= 'establishment_save_error';


//TICKETS
export const FETCH_TICKET_LOADING = 'fetch_tickets_loading';
export const FETCH_TICKETS = 'fetch_tickets';
export const FETCH_TICKETS_ERROR = 'fetch_tickets_error';

export const FETCH_FAVORIT_TICKETS_LOADING = 'fetch_favorit_tickets_laoding';
export const FETCH_FAVORIT_TICKETS = 'fetch_favorit_tickets';
export const FETCH_FAVORIT_TICKETS_ERROR = 'fetch_favorit_tickets_error';

export const FETCH_ESTABLISHMENT_TICKETS = 'fetch_establishment_tickets';
export const FETCH_ACTIVE_TICKET = 'fetch_active_ticket';
export const TICKET_UPDATE = 'ticket_update'
export const IMAGE_UPDATE = 'image_update'
export const DATE_UPDATE = 'date_update'
export const TICKET_LOADING = 'ticket_loading'
export const TICKET_SAVE_SUCCESS = 'ticket_save_sucess'
export const TICKET_SAVE_ERROR = 'ticket_save_error'
export const CLEAR_FOUND_ITENS_LIST = 'clear_found_itens_list'

//PRODUCTS
export const SET_PRODUCT = 'set_product';
export const PRODUCT_UPDATE = 'product_update';
export const FETCH_PRODUCTS_LOADING = 'fetch_products_loading';
export const FETCH_PRODUCTS = 'fetch_products';
export const FETCH_DESTAQUE_PRODUCTS = 'fetch_destaque_products';
export const FETCH_PRODUCTS_ERROR = 'fetch_products_error';

//CATERGORIES
export const CATEGORY_UPDATE = 'category_update';
export const FETCH_CATEGORY_LOADING = 'fetch_categories_loading';
export const FETCH_CATEGORY = 'fetch_categories';
export const FETCH_CATEGORY_PRODUCTS = 'fetch_categories_products';
export const FETCH_CATEGORY_ERROR = 'fetch_category_error';


//STAMPS
export const FETCH_STAMPS = 'fetch_stamps';
export const REQUEST_STAMP = 'request_stamp';


//LOGOUT
export  const LOGOUT ='logout'

