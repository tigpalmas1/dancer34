import {
    AUTH_UPDATE, CLEAR_USER, LOGIN_LOADING, LOGIN_ERROR, LOGIN_SUCCESS, SIGNUP_LOADING, SIGNUP_SUCCESS, SIGNUP_ERROR,
    LOGOUT, AUTH_LOADING, FETCH_ESTABLISHMENT, FETCH_ACTIVE_TICKET, ESTABLISHMENT_UPDATE, TICKET_UPDATE

} from "./types";
import {getStorage, setStorage} from "../../components/common/functions/index";
import Parse from 'parse/react-native';

export const authUpdate = ({prop, value}, password) => async (dispatch) => {
    dispatch({
        type: AUTH_UPDATE,
        payload: {prop, value}
    })
};


export const checkLoggedAction = () => async (dispatch) => {
    dispatch({type: AUTH_LOADING})


    try {
        const user = await getStorage("user");
        var ParseUser = Parse.Object.extend("User");
        var query = new Parse.Query(ParseUser);
        query.include('establishmentId')
        query.include('activeTicketId')
        const userServer = await query.get(user.objectId)

        if (user) {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: {
                    user: userServer,
                    userJson: userServer.toJSON()
                }
            })

        } else {
            dispatch({type: LOGIN_ERROR})
        }

        if (userServer.has('establishmentId')) {
            var establishment = userServer.get('establishmentId')

            dispatch({type: FETCH_ESTABLISHMENT, payload: establishment.toJSON()})
        }
        if (userServer.has('activeTicketId')) {

            var ticket = userServer.get('activeTicketId')

            dispatch({type: FETCH_ACTIVE_TICKET, payload: ticket.toJSON()})
        }
    } catch (e) {
        console.log(e)
        dispatch({type: LOGIN_ERROR})
    }


};

export const loginServerAction = (auth) => async (dispatch) => {
    dispatch({type: LOGIN_LOADING})
    try {
        var user = new Parse.User(auth);
        const serveruser = await user.logIn()

        await setStorage("user", serveruser);

        dispatch({
            type: LOGIN_SUCCESS,
            payload: serveruser,
        })

    } catch (e) {
        console.log(e.message)
        dispatch({
            type: LOGIN_ERROR,
            payload: e.message
        })
    }
};


export const signupServer = (auth) => async (dispatch) => {
    auth.confirmPassword = undefined

    dispatch({type: SIGNUP_LOADING})
    try {
        var user = new Parse.User(auth);
        const serveruser = await user.signUp()

        await setStorage("user", serveruser);

        dispatch({
            type: SIGNUP_SUCCESS,
            payload: serveruser,

        })

    } catch (e) {
        console.log(e)
        dispatch({
            type: SIGNUP_ERROR,
            payload: e.message
        })
    }
};


export const logoutAction = () => async (dispatch) => {

    try {
        await setStorage("user", null);
        dispatch({type: LOGOUT})

    } catch (e) {
        alert(e.message)
    }


};







