import {
    CLEAR_FOUND_ITENS_LIST,
    DATE_UPDATE,
    ESTABLISHMENT_LOADING, ESTABLISHMENT_SAVE_ERROR, ESTABLISHMENT_SAVE_SUCCESS,

    ESTABLISHMENT_UPDATE, FETCH_DESTAQUE_PRODUCTS, FETCH_ESTABLISHMENT_TICKETS, FETCH_EVENTS, FETCH_EVENTS_LOADING,
    FETCH_FAVORIT_TICKETS,
    FETCH_FAVORIT_TICKETS_ERROR,
    FETCH_FAVORIT_TICKETS_LOADING, FETCH_PRODUCTS, FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_LOADING,
    FETCH_STAMPS, FETCH_TICKET_LOADING,
    FETCH_TICKETS, FETCH_TICKETS_ERROR, IMAGE_UPDATE,
    LOGIN_SUCCESS, PRODUCT_UPDATE, SET_PRODUCT, TICKET_LOADING,
    TICKET_SAVE_ERROR,
    TICKET_UPDATE, UPDATER_USER_LOCATION,

} from "./types";
import {getStorage, setStorage} from "../../components/common/functions/index";
import Parse from 'parse/react-native';
import {Location, Permissions, IntentLauncherAndroid} from 'expo';
import _ from 'lodash'


export const fetch_events = () => async (dispatch) => {


    dispatch({type: FETCH_EVENTS_LOADING, payload: {loading: true, message: 'Buscando Eventos'}})

    try {
        const EventClass = Parse.Object.extend('Event');
        const query = new Parse.Query(EventClass);
        const results = await query.find();

        const jsonResults = _.map(results, function (x) {
            return x.toJSON()
        });

        console.log(jsonResults)
        dispatch({
            type: FETCH_EVENTS,
            payload: {
                products: jsonResults,
                message: jsonResults.length === 0 ? 'Nenhum Evento Encontrado' : ''
            }
        })
    }
    catch
        (e) {
        console.log(e.message)
        dispatch({type: FETCH_EVENTS_LOADING, payload: {loading: false, message: 'ops, algo deu erraodo ' + e.message}})
    }
}









