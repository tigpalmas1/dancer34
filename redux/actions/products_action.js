import {
    CLEAR_FOUND_ITENS_LIST,
    DATE_UPDATE,
    ESTABLISHMENT_LOADING, ESTABLISHMENT_SAVE_ERROR, ESTABLISHMENT_SAVE_SUCCESS,

    ESTABLISHMENT_UPDATE, FETCH_DESTAQUE_PRODUCTS, FETCH_ESTABLISHMENT_TICKETS, FETCH_FAVORIT_TICKETS,
    FETCH_FAVORIT_TICKETS_ERROR,
    FETCH_FAVORIT_TICKETS_LOADING, FETCH_PRODUCTS, FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_LOADING,
    FETCH_STAMPS, FETCH_TICKET_LOADING,
    FETCH_TICKETS, FETCH_TICKETS_ERROR, IMAGE_UPDATE,
    LOGIN_SUCCESS, PRODUCT_UPDATE, SET_PRODUCT, TICKET_LOADING,
    TICKET_SAVE_ERROR,
    TICKET_UPDATE, UPDATER_USER_LOCATION,

} from "./types";
import {getStorage, setStorage} from "../../components/common/functions/index";
import Parse from 'parse/react-native';
import {Location, Permissions, IntentLauncherAndroid} from 'expo';
import _ from 'lodash'

export const productUpdate = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: PRODUCT_UPDATE,
        payload: {prop, value}
    })
};


export const setProductAction = (product) => async (dispatch) => {
    dispatch({type: SET_PRODUCT, payload: product})
};

export const imageProductUpdate = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: IMAGE_UPDATE,
        payload: {prop, value}
    })
};


export const fetch_products = (categoryId) => async (dispatch) => {


    dispatch({type: FETCH_PRODUCTS_LOADING, payload: true})

    try {
        const CategoryClass = Parse.Object.extend('Category');
        let categoryObject = new CategoryClass();
        categoryObject.id = categoryId;


        const Products = Parse.Object.extend("Product");
        const query = new Parse.Query(Products);
        query.equalTo('categoryId', categoryObject)
        const results = await query.find();

        const jsonResults = _.map(results, function (x) {
            return x.toJSON()
        });




        dispatch({
            type: FETCH_PRODUCTS,
            payload: {
                products: jsonResults,
                message: jsonResults.length === 0 ? 'Nenhum Produto Encontrado' : ''
            }
        })


    }
    catch
        (e) {
        console.log(e.message)
        dispatch({
            type: FETCH_PRODUCTS_ERROR,
            payload: 'Ops, algo deu errado, tente novamente ' + e.message
        })
    }
}

export const saveProductAction = (product, categoryId, callback) => async (dispatch) => {
    console.log(categoryId)

    try {
        const {name, description, price, available, highlight, result} = product || {}

        const ProductClass = Parse.Object.extend('Product');
        let productObject = new ProductClass();

        productObject.id = product.objectId;

        if (result) {
            console.log('entrou alterar foto')
            var fileResult = new Parse.File("productFile.png", {base64: result.base64});
            productObject.set('imageProduct', fileResult);
        }

        productObject.set('name', name)
        productObject.set('description', description)
        productObject.set('price', price)
        productObject.set('available', available)
        productObject.set('highlight', highlight)
        await productObject.save()

        callback('produto salvo com sucesso')

    }
    catch (e) {
        callback('algo deu errado' + e.message)
    }
}











