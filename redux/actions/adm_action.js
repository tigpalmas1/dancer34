import {
    ADD_USER, ADD_USER_LOADING, AUTH_UPDATE, CLEAR_USER, FETCH_STYLE, FETCH_USERS, FETCH_USERS_LOADING,
    IMAGE_DANCER_UPDATE,
    IMAGE_USER_UPDATE,
    SET_UPDATE_USER, UPDATE_USER,
    USER_INFO_UPDATE
} from "./types";

import Parse from 'parse/react-native';
import _ from 'lodash'



export const fetch_styles = () => async (dispatch) => {

    try {

        const Style = Parse.Object.extend("Style");
        const queryStyle = new Parse.Query(Style);
        const resultStyles = await queryStyle.find();

        const resultStylesJosn = await _.map(resultStyles, function (item) {
            return item.toJSON()
        })



       console.log(resultStyles)


        dispatch({
            type: FETCH_STYLE,
            payload:  resultStylesJosn,

        })
    }
    catch
        (e) {
        console.log(e.message)
      /*  dispatch({
            type: FETCH_PRODUCTS_ERROR,
            payload: 'Ops, algo deu errado, tente novamente ' + e.message
        })*/
    }
}













