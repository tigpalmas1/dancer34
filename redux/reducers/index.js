import {combineReducers} from 'redux';


import auth from './auth_reducer'

import product from './product_reducer'
import category from './category_reducer'
import user from './user_reducer'
import dancer from './dancer_reducer'
import adm from './administration_reducer'
import event from './events_reducer'


export default combineReducers({
    auth,
    adm,
    event,
    user,
    dancer,
    product,
    category
});