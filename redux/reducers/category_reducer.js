import {} from "../actions/types";
import _ from 'lodash';
import {ESTABLISHMENT_UPDATE} from "../actions/types";
import {LOGOUT} from "../actions/types";
import {FETCH_ESTABLISHMENT} from "../actions/types";
import {ESTABLISHMENT_IMAGE_UPDATE} from "../actions/types";
import {FETCH_PRODUCTS} from "../actions/types";
import {FETCH_CATEGORY} from "../actions/types";
import {CATEGORY_UPDATE} from "../actions/types";
import {FETCH_CATEGORY_PRODUCTS} from "../actions/types";


const INITIAL_STATE = {

    categories: [],
    categories_products:[],
    category: {
        name:''
    },
    errorMessage: '',
    loading: false,
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case CATEGORY_UPDATE:
            return {
                ...state,
                category: {
                    ...state.category,
                    [action.payload.prop]: action.payload.value
                },

            }

        case FETCH_CATEGORY:
            return {
                ...state,
                categories:action.payload.categories,
                message: action.payload.message,
                loading: false,
            }
        case FETCH_CATEGORY_PRODUCTS:
            return {
                ...state,
                categories_products:action.payload.categories_products,
                message: action.payload.message,
                loading: false,
            }
        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

