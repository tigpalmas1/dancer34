import {} from "../actions/types";
import _ from 'lodash';
import {ESTABLISHMENT_UPDATE} from "../actions/types";
import {LOGOUT} from "../actions/types";
import {FETCH_ESTABLISHMENT} from "../actions/types";
import {ESTABLISHMENT_IMAGE_UPDATE} from "../actions/types";
import {FETCH_PRODUCTS} from "../actions/types";
import {SET_PRODUCT} from "../actions/types";
import {PRODUCT_UPDATE} from "../actions/types";
import {FETCH_DESTAQUE_PRODUCTS} from "../actions/types";
import {IMAGE_UPDATE} from "../actions/types";
import {FETCH_EVENTS} from "../actions/types";


const INITIAL_STATE = {
    events: [],
    message: '',
    loading: false,
}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {


        case FETCH_EVENTS:
            return {
                ...state,
                events:payload.products,
                message: payload.message,
                loading: false,
            }

        case FETCH_DESTAQUE_PRODUCTS:
            return {
                ...state,
                events: _.uniqBy(state.kits.concat(payload.events), "objectId"),
                loading: false,
                message: payload.message,
            }



        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

