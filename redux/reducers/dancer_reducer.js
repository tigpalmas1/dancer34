import {
    ADD_USER, ADD_USER_LOADING, AUTH_UPDATE, CLEAR_USER, FETCH_USERS, FETCH_USERS_LOADING, LOGIN_SUCCESS, LOGOUT,
    SET_UPDATE_USER, SIGNUP_SUCCESS, UPDATER_USER_LOCATION, IMAGE_USER_UPDATE, IMAGE_DANCER_UPDATE, DANCER_INFO_UPDATE
} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    user: '',
    perfilDancer: {
        description: '',
        level: '',
        selectedStyles: [],
        image1: {
            url: 'https://www.palladiumhotelgroup.com/_ui/responsive/theme-palladium/images/default-img.png'
        },
        image2: {
            url: 'https://www.palladiumhotelgroup.com/_ui/responsive/theme-palladium/images/default-img.png'
        },
        image3: {
            url: 'https://www.palladiumhotelgroup.com/_ui/responsive/theme-palladium/images/default-img.png'
        },
        image4: {
            url: 'https://www.palladiumhotelgroup.com/_ui/responsive/theme-palladium/images/default-img.png'
        },
        image5: {
            url: 'https://www.palladiumhotelgroup.com/_ui/responsive/theme-palladium/images/default-img.png'
        },
        image6: {
            url: 'https://www.palladiumhotelgroup.com/_ui/responsive/theme-palladium/images/default-img.png'
        }
    },


}

export default function (state = INITIAL_STATE, {type, payload}) {
    switch (type) {


        case IMAGE_DANCER_UPDATE:

            return {
                ...state,
                perfilDancer: {
                    ...state.perfilDancer,
                    [payload.prop]: {
                        url: payload.value
                    }

                }
            }
        case DANCER_INFO_UPDATE:
            return {
                ...state,
                perfilDancer: {
                    ...state.perfilDancer,
                    [payload.prop]: payload.value
                },
            }


        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

