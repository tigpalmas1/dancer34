import {
    ADD_USER, ADD_USER_LOADING, AUTH_UPDATE, CLEAR_USER, FETCH_USERS, FETCH_USERS_LOADING, LOGIN_SUCCESS, LOGOUT,
    SET_UPDATE_USER, SIGNUP_SUCCESS, UPDATER_USER_LOCATION, IMAGE_USER_UPDATE
} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    user: '',
    userJson: {
        userImage: {
            url: 'https://static.wixstatic.com/media/926bfc_7da31dbfb03c4cd3b796533b227687a3~mv2.gif'
        },
    },
    userLocation: ''

}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SIGNUP_SUCCESS:
            return {
                ...state,
                user: action.payload
            }
        case LOGIN_SUCCESS:

            return {
                ...state,
                user: action.payload.user,
                userJson: action.payload.userJson,


            }

        case UPDATER_USER_LOCATION:
            return {
                ...state,
                userLocation: action.payload
            }

        case IMAGE_USER_UPDATE:
            console.log(action.payload)

            return {
                ...state,
                userJson: {
                    ...state.userJson,
                    userImage: {
                        [action.payload.prop]: action.payload.value
                    }

                }
            }


        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

