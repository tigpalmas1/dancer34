import {} from "../actions/types";
import _ from 'lodash';
import {ESTABLISHMENT_UPDATE} from "../actions/types";
import {LOGOUT} from "../actions/types";
import {FETCH_ESTABLISHMENT} from "../actions/types";
import {ESTABLISHMENT_IMAGE_UPDATE} from "../actions/types";
import {FETCH_PRODUCTS} from "../actions/types";
import {SET_PRODUCT} from "../actions/types";
import {PRODUCT_UPDATE} from "../actions/types";
import {FETCH_DESTAQUE_PRODUCTS} from "../actions/types";
import {IMAGE_UPDATE} from "../actions/types";


const INITIAL_STATE = {
    product: {
        name: '',
        description: '',
        value: '',
        imageProduct: {
            url: 'https://static.wixstatic.com/media/926bfc_7da31dbfb03c4cd3b796533b227687a3~mv2.gif'
        },
        category: '',
        available: true,
        highlight: false,
    },
    products: [],
    destaqueProducts: [],
    errorMessage: '',
    loading: false,
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {


        case SET_PRODUCT:
            return {
                ...state,
                product:action.payload,
            }
        case PRODUCT_UPDATE:
            return {
                ...state,
                product: {
                    ...state.product,
                    [action.payload.prop]: action.payload.value
                },
            }

        case IMAGE_UPDATE:
            return {
                ...state,
                product: {
                    ...state.product,
                    imageProduct: {
                         [action.payload.prop]: action.payload.value
                    }

                },

            }


        case FETCH_PRODUCTS:
            return {
                ...state,
                products:action.payload.products,
                message: action.payload.message,
                loading: false,
            }

        case FETCH_DESTAQUE_PRODUCTS:
            return {
                ...state,
                destaqueProducts:action.payload.destaqueProducts,
                message: action.payload.message,
                loading: false,
            }



        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

