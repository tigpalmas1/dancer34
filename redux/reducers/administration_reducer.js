import {
    ADD_USER, ADD_USER_LOADING, AUTH_UPDATE, CLEAR_USER, FETCH_USERS, FETCH_USERS_LOADING, LOGIN_SUCCESS, LOGOUT,
    SET_UPDATE_USER, SIGNUP_SUCCESS, UPDATER_USER_LOCATION, IMAGE_USER_UPDATE, IMAGE_DANCER_UPDATE, FETCH_CITY,
    FETCH_STYLE
} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    user: '',
    styles: [],
    city: [],

}

export default function (state = INITIAL_STATE, {type, payload}) {
    switch (type) {


        case FETCH_STYLE:
            return {
                ...state,
                styles: _.uniqBy(state.styles.concat(payload), "objectId"),
            }


        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

