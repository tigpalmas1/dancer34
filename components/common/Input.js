import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "./colors/index";
import {errorMessageStyle} from "./styles/perfilStyle";


const Input = ({label, value, onChangeText, multiline, placeholder, secureTextEntry,
                   editable,
                   error, setRef, onSubmitEditing, autoCapitalize}) => {

    const {inputStyle, labelStyle, containerStyle, inputWrapper} = styles;

    return (
        <View style={containerStyle}>
            {label &&
            <Text style={[labelStyle,{color: error? 'red': titleGrey}]}>{label}</Text>

            }
            <Text style={errorMessageStyle}>{error}</Text>


            <View style={[inputWrapper, {  borderColor:error? 'red':  purpleTitle,}]}>
                <TextInput
                    editable={editable}
                    ref={setRef}
                    secureTextEntry={secureTextEntry}
                    autoCapitalize={autoCapitalize}
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    onSubmitEditing={onSubmitEditing}
                    placeholder={placeholder}
                    multiline={multiline}
                    value={value}

                    onChangeText={onChangeText}
                    style={inputStyle}/>
            </View>




        </View>
    )
}

const styles = {
    containerStyle: {
    flex: 1,
        marginTop: 5,
    },
    inputWrapper: {
        marginHorizontal: 3,

        borderBottomWidth: 0.5,

        padding: 3,

    },
    inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        color: titleColor,
        fontSize: 16,
        fontWeight: '700'
    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',


    },

}

export {Input};