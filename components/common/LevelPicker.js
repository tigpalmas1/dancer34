import React, {Component} from 'react';
import {
    View, Text, TouchableOpacity
} from 'react-native';
import {tag, tagWrapper} from "./styles/geralStyle";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {grey, purpleTitle, titleColor, titleGrey} from "./colors/index";


const LevelPicker = ({levelUpdate, level, levelError}) => {
    const {pickerContainer} = styles;

    return (

        <View style={{marginTop:0}}>

            <Text style={{
                color: levelError? 'red': titleGrey,
                marginBottom: 5,
                fontSize: 16,
                fontWeight: '700',
            }}>{'Nível de Conhecimento'}</Text>
            <View style={pickerContainer}>
                <TouchableOpacity onPress={() => levelUpdate('iniciante')}>
                    <View
                        style={[tagWrapper, {backgroundColor: level === 'iniciante' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Iniciante</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => levelUpdate('intermediário')}>
                    <View
                        style={[tagWrapper, {backgroundColor: level === 'intermediário' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Intermediário</Text>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => levelUpdate('avançado')}>
                    <View
                        style={[tagWrapper, {backgroundColor: level === 'avançado' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Avançado</Text>
                    </View>

                </TouchableOpacity>

            </View>

            <View style={[pickerContainer, {marginTop: 5}]}>

                <TouchableOpacity onPress={() => levelUpdate('professor')}>
                    <View
                        style={[tagWrapper, {backgroundColor: level === 'professor' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Professor</Text>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => levelUpdate('personal dancer')}>
                    <View
                        style={[tagWrapper, {backgroundColor: level === 'personal dancer' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Personal Dancer</Text>
                    </View>

                </TouchableOpacity>

            </View>
            <Text style={{color: 'red', fontWeight: '700', fontSize: 8}}>{levelError}</Text>
        </View>


    )
}

const styles = {
    pickerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center'
    }
}

export {LevelPicker};

