import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "./colors/index";
import {errorMessageStyle} from "./styles/perfilStyle";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


const CheckBox = ({
                      label, checked, onPressCheck, value, onChangeText, multiline, placeholder, secureTextEntry,
                      editable,
                      error, setRef, onSubmitEditing, autoCapitalize
                  }) => {

    const {inputStyle, labelStyle, containerStyle, inputWrapper} = styles;

    return (
        <View style={containerStyle}>

            <TouchableOpacity
                onPress={onPressCheck}
                style={{flexDirection: 'row'}}>
                {label &&
                <Text style={[labelStyle, {color: error ? 'red' : titleGrey}]}>{label}</Text>

                }

                <Icon
                    size={24}
                    name={checked ? 'checkbox-marked' : 'checkbox-blank-outline'}
                    style={{color: error ? 'red' : 'grey', flexWrap: 'wrap'}}
                />
            </TouchableOpacity>


            <Text style={errorMessageStyle}>{error}</Text>


        </View>
    )
}

const styles = {
    containerStyle: {

        flex: 1,
        marginTop: 5,
    },
    inputWrapper: {
        marginHorizontal: 3,

        borderBottomWidth: 0.5,

        padding: 3,

    },
    inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        color: titleColor,
        fontSize: 16,
        fontWeight: '700'
    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',


    },

}

export {CheckBox};