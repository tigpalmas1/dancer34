export const  facebookBlue =  '#3b5998'
export const  lightPurple =   '#ef7aac'
export const  purpleTitle =   '#FA036B'
export const  darkPurple =   '#9b0142'
export const    grey= "#eae5e5"
export const    titleGrey= "#96a5ac"
export const    darkGrey= "#454A58"
export const    titleColor= "#1F1E24"