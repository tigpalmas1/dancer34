import React, {Component} from 'react';
import {Text,  TouchableOpacity, View} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {darkPurple, lightPurple, purpleTitle} from "./colors/index";
import {tag, tagWrapper} from "./styles/geralStyle";



const TagButton = ({title,  onPress, color}) => {


    return (
        <TouchableOpacity onPress={onPress}>
            <View>
                <View
                    style={[tagWrapper, {backgroundColor: color}]}>
                    <Text style={tag}>{title}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = {}

export {TagButton};