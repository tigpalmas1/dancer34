import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import {grey, purpleTitle, titleGrey} from "./colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'



const InputLogin = ({label, error, value, onChangeText, placeholder, secureTextEntry, icon, autoCapitalize,  setRef, onSubmitEditing}) => {

    const {inputStyle, labelStyle, containerStyle, inputWrapper, } = styles;

    return (
        <View style={containerStyle}>




            <View style={[inputWrapper, { borderColor: error? 'red': purpleTitle}]}>
                <Icon
                    size={24}
                    name={icon}
                    style={ {color: error? 'red':  'grey', flexWrap:'wrap'} }
                />

                <TextInput
                    secureTextEntry={secureTextEntry}
                    autoCapitalize={autoCapitalize}
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    placeholder={placeholder}
                    placeholderTextColor={grey}
                    value={value}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                    ref={setRef}
                    blurOnSubmit={false}
                    style={inputStyle}/>
            </View>
            <Text style={{color: 'red', fontWeight: '700', fontSize: 8}}>{error}</Text>

        </View>
    )
}

const styles = {
    containerStyle: {
        flex: 1,


    },
    inputWrapper: {
        width: '100%',
        flexDirection: 'row',
        margin: 3,
        borderBottomWidth: 1,

        padding: 5,

    },
    inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        flex: 1,

        color: 'black',
        fontSize: 16,
        fontWeight: '700'
    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',


    },

}

export {InputLogin};