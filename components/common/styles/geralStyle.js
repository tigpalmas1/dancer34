import {grey, purpleTitle, titleColor, titleGrey} from "../colors/index";


//GERAL STYLES

export const tag =  {fontSize: 14, color: 'white', fontWeight: '700'}
export const tagWrapper =  {padding: 5, backgroundColor: purpleTitle, flexWrap: 'wrap', borderRadius: 12,  marginRight: 5, marginTop: 5,   flexDirection: 'row', alignItems:'center'}
export const regularText =  {fontSize: 15, color: 'grey'}
export const textSubTitle=  {fontSize: 22, fontWeight: '900', lineHeight: 31, color: titleColor}
export const seeMoreStyle=  {width: '100%', alignItems: 'flex-end', marginTop: 10}
export const textSeeMoreStyle=  {color: purpleTitle, fontWeight: '900',}
export const vipListContainer=  {position: 'absolute',  right: 10, bottom: 10,

    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,}
export const  emptyStateContainer =   {flex:1, alignItems:'center', justifyContent:'center', marginTop:100, marginBottom:100}
export const  emptyContainer =  {position: 'absolute',top: 100, left: 0, right: 0, bottom: 100, justifyContent: 'center', alignItems: 'center'}
export const  titleFragments =   {fontSize: 24, fontWeight: '700', lineHeight:23, color: titleColor}
export const  subTitleContainer =   {width: '100%', alignItems: 'center', flexDirection:'row', justifyContent: 'space-between'}
export const  subTitleFragments =   { fontSize: 12, color: titleGrey, paddingBottom: 2}
export const  seeMoreContainer =  {paddingBottom: 2}
export const  seeMoreButton =   {color: purpleTitle, fontWeight: '900', fontSize: 13}


//Login Signup
export const messageStyle =  {
    paddingHorizontal: 20,
        textAlign: "center",
        marginTop: 10,
        fontWeight: '700',
        fontSize: 14,
        color: titleGrey
}


export const perfilTitleStyle = {
    color:  titleGrey,
    marginBottom: 5,
    fontSize: 16,
    fontWeight: '700',
}

