import {grey, purpleTitle, titleGrey} from "../colors/index";

/*Card Style*/
export const  imageStyle=  {flex: 1, width: null, height: null, resizeMode: 'cover', borderTopLeftRadius: 8, borderTopRightRadius: 8}
export const  imageContainer= {flex: 1, width: null,height: null, borderTopLeftRadius: 8, overflow: 'hidden', borderTopRightRadius: 8}
export const  avatarEstablishment=  {width: 50, height: 50, position: 'absolute', right: 5, top: 5, borderRadius: 25,  borderWidth: 1, borderColor: 'white'}
export const  cardsWrapper =  { flex: 1, height: 200, width: 250,marginTop: 10,  marginRight: 10, borderRadius: 8, borderWidth: 1, borderColor: '#dddddd'}