import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colors from "./colors";
import {darkPurple, lightPurple, purpleTitle} from "./colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {LinearGradient} from 'expo';


const PerfilButton = ({onPress, label}) => {

    return (

        <TouchableOpacity
            onPress={onPress}>
            <View style={{marginTop: 10}}><Text
                style={{fontSize: 18, fontWeight: '700', color: 'grey'}}>{label}</Text>
            </View>
        </TouchableOpacity>
    )
}



export {PerfilButton};