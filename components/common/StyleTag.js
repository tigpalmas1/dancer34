import React, {Component} from 'react';
import {Text,  TouchableOpacity, View} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {darkPurple, lightPurple, purpleTitle, titleGrey} from "./colors/index";
import {tag, tagWrapper} from "./styles/geralStyle";
import _ from 'lodash'


const StyleTag = ({el, selectedStyles, styleUpdate}) => {

    const found = _.find(selectedStyles,el);

    return (
        <TouchableOpacity onPress={() => styleUpdate(el)}>
            <View
                style={[tagWrapper, {backgroundColor:  found ? purpleTitle : titleGrey}]}>
                <Text style={tag}>{el.name}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = {}

export {StyleTag};