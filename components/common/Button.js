import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colors from "./colors";
import {darkPurple, lightPurple, purpleTitle} from "./colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { LinearGradient } from 'expo-linear-gradient';

const Button = ({label, onPress, color, disabled, textColor, leftIcon}) => {
    const {buttonStyle, textStyle} = styles;

    return (

        <LinearGradient
            colors={disabled ? ['grey', 'grey'] : [lightPurple, purpleTitle, darkPurple]}
            start={[0, 0]}
            end={[1, 0]}
            style={{
                borderRadius: 8, marginLeft: 5,
                marginRight: 5,
            }}>
            <TouchableOpacity
                onPress={disabled ? null : onPress} style={[buttonStyle,]}>

                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    {leftIcon &&
                    < Icon
                        size={24}
                        name={leftIcon}
                        style={{color: 'white', marginRight: 5}}
                    />
                    }

                    <Text style={[textStyle, {color: textColor || 'white',}]}>{label}</Text>
                </View>


            </TouchableOpacity>
        </LinearGradient>
    )
}

const styles = {
    textStyle: {
        alignSelf: 'center',

        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        fontWeight: '900'
    },
    buttonStyle: {
        paddingHorizontal: 10,
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',

        borderRadius: 12,

    }
}

export {Button};