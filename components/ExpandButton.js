import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {titleColor} from "./common/colors/index";




const ExpandButton = ({label, onPress, expanded    }) => {
    const {buttonStyle, textStyle} = styles;


    return (

        <TouchableOpacity
            style={{flexWrap: 'wrap', flexDirection: 'row', alignItems: 'center'}}
            onPress={onPress}
        >
            <Text style={{fontSize: 18, fontWeight: '900', color: titleColor}}>{label}</Text>
            <Icon
                style={{marginRight: 5, marginLeft: 5}}
                size={24}
                color={'black'}
                name={expanded ? "menu-down" : "menu-up"}
            />
        </TouchableOpacity>
    )
}

const styles = {

}

export {ExpandButton};