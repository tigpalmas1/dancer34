import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {grey, purpleTitle} from "../components/common/colors/index";

export default function TabBarIcon(props) {
    return (
        <Icon
            size={24}
            name={props.name}
            color={props.focused ? purpleTitle: grey}
        />
    );
}
