import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Platform, StatusBar, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import {purpleTitle, titleColor} from "./common/colors/index";
import {InputHeader} from "./common/InputHeader";


class Header extends React.Component {


    constructor(props) {
        super(props)
        this.state = ({
            searchBarVisible: false,
            inputValue: ''
        })

    }


    renderBackButton = (backButton, onPress) => {

        if (backButton) {
            return (
                <TouchableOpacity onPress={onPress}>
                    <Icon
                        style={{marginRight: 10}}
                        color={'white'}
                        size={24}
                        name="arrow-left"
                    />
                </TouchableOpacity>
            )
            return (
                null
            )
        }

    }


    render() {
        const {header, inputWrapper} = styles;
        const {searchBarVisible, inputValue} = this.state;
        const {
            title, onPress, backButton, rigthButton, loadingMap, mapIcon, onPressMap, onPressRigth, onPressSearch,
            iconRigthName,
            searchBar,
            searchBarIcon,
            notificationButton,
            onPressNotification
        } = this.props


        return (


            <View style={[header, {marginTop: searchBar ? 20 : 20, marginBottom: searchBar? 10: 0}]}>

                <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                    {this.renderBackButton(backButton, onPress)}


                    {!searchBar &&
                    <Text style={styles.title}>{title}</Text>
                    }

                    {searchBar &&


                    <View style={{ flex: 1, elevation: 5 }}>

                        <InputHeader
                            onPressClose={this.props.onPressClose}
                            onSubmitEditing={this.props.onSubmitEditing}

                            onChangeText={value => {
                                this.setState({inputValue: value})
                                this.props.onChangeText(value)
                            }}
                            value={inputValue}/>

                    </View>


                    }

                </View>


                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>



                </View>

            </View>

        )

    }


}

const styles = {
    header: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20,
        height: 55,


    },
    title: {
        color: '#fff',
        fontSize: 20,
        fontWeight: '700',
    }


}

export {Header};