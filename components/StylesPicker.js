import React, {Component} from 'react';
import {
    View, Text, TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {purpleTitle, titleGrey} from "./common/colors/index";
import {tagWrapper, tag, perfilTitleStyle} from "./common/styles/geralStyle";
import {StyleTag} from "./common/StyleTag";



const StylesPicker = ({styleUpdate, estilos, levelError, selectedStyles}) => {
    const {pickerContainer} = styles;



    return (

        <View style={{marginTop:0, flex: 1}}>

            <Text style={perfilTitleStyle}>{'Estilos'}</Text>
            <View style={pickerContainer}>

                {estilos.map(el => (
                    <StyleTag
                        key={el.objectId}
                        styleUpdate={styleUpdate} selectedStyles={selectedStyles} el={el} />


                ))}




            </View>

            <Text style={{color: 'red', fontWeight: '700', fontSize: 8}}>{levelError}</Text>
        </View>


    )
}

const styles = {
    pickerContainer: {
        flex: 1,
       flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center'
    }
}

export {StylesPicker};

