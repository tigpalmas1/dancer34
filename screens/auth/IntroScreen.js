import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    Linking,
    StatusBar,
    ActivityIndicator
} from 'react-native';
import {connect} from 'react-redux'
import {StackActions, NavigationActions} from 'react-navigation';
import {grey, purpleTitle, titleColor} from "../../components/common/colors/index";
import {cancelButtonStyle} from "../../components/common/styles/dialogStyles";


const {height, width} = Dimensions.get('window');

class IntroScreen extends React.Component {
    static navigationOptions = {header: null};

    constructor(props) {
        super(props);
        this.state = {
            action: '',
            updateDialog: false,
            filterDialog: false,
            titleUpdate: '',
            textUpdate: '',
            loading: true,
            message: ''
        };
        this.navigateHome()

    }




    navigateHome = () =>{
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'main'})],
        });
        this.props.navigation.dispatch(resetAction);
    };



    render() {

        const { container, footerTextContainer, footerTextStyle, titleContainer, spinnerContainer} = styles || {};

        const {updateDialog, titleUpdate, textUpdate, message, loading, filterDialog} = this.state || {};

        return (
            <View style={container}>
                <StatusBar hidden={true}/>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>



                </View>


                <View style={titleContainer}>
                    <Text style={{color: purpleTitle, fontSize: 38, fontWeight: '900'}}>Step IN</Text>


                </View>


                {message !==  '' &&
                <View style={{position: 'absolute',  bottom: 100, right: 0, left: 0, alignItems: 'center', justifyContent: 'center'}}>
                    <TouchableOpacity
                        onPress={()=>  {
                            this.setState({loading: true});
                            this.props.checkVersion(this.callBackVersion)}}
                    >
                        <Text style={cancelButtonStyle}>Tentar Novamente</Text>
                    </TouchableOpacity>
                </View>

                }





                {loading &&
                <View style={ spinnerContainer}>
                    <ActivityIndicator
                        color={purpleTitle}
                        size='large'/>
                </View>


                }



            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',

    },

    loading: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',

    },
    footerTextContainer: {
        position: 'absolute',
        bottom: 50,
        left: 0,
        right: 0,
        alignItems: 'center',
        flexWrap: 'wrap',},
    footerTextStyle: {color: 'grey', fontSize: 12, fontWeight: '700'},
    titleContainer: {
        position: 'absolute',
        top: 100,
        left: 50,
        flexWrap: 'wrap',
        width: '100%',
        height: 100
    }, spinnerContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',

    }

    });


const mapStateToProps = (state) => {


    return {
        checked: state.auth.checked,
        logged: state.user.logged,
        user: state.user.user,
        loading: state.user.loading,

    };
};


export default connect(mapStateToProps, {})(IntroScreen)
