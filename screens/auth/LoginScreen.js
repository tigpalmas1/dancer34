import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Dimensions,
    Picker,
    ScrollView,
    Keyboard,
    RefreshControl,
    TouchableOpacity
} from 'react-native';
import {Button, CardSection, LoadingItem, TransparentHeader} from "../../components/common";
import {connect} from 'react-redux'
import {authUpdate, loginServer, facebookLogin} from "../../redux/actions";
import {InputLogin} from "../../components/common/InputLogin";
import {darkGrey, facebookBlue, purpleTitle, titleGrey} from "../../components/common/colors/index";
import {titleStyle} from "../../components/common/styles/loginSignupStyle";
import {messageStyle, seeMoreContainer} from "../../components/common/styles/geralStyle";
import {PASSWORD_RECOVER} from "../../components/common/constants/index";


const {height, width} = Dimensions.get('window')

class LoginScreen extends React.Component {
    static navigationOptions = {header: null}


    constructor(props) {
        super(props)
        this.state = {
            emailError: '',
            passwordError: '',
        };
    }


    componentDidUpdate() {
        console.log(this.props.loginExpired)
        if(this.props.loginExpired){
            this.props.navigation.navigate('resetPassword')
        }
        if (this.props.logged) {
            this.props.navigation.goBack(null)
        }

    }

    onLoginPress = () => {

        if (!this.checkError()) {
            this.props.loginServer(this.props.auth)
        }
    }


    checkError() {
        const {email, password} = this.props.auth || {}
        let errors = false;

        if (!email) {
            this.setState({emailError: 'Email obrigátorio'})
            errors = true
        }
        if (email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            this.setState({emailError: 'Email está no formato incorreto'})
            errors = true
        }

        if (!password) {
            this.setState({passwordError: 'Senha obrigátoria'})
            errors = true
        }
        if (password && password.length < 8 || password.length > 14) {
            this.setState({passwordError: 'Senha deve ter entre 8 e 14 caracteres'})
            errors = true
        }
        return errors

    }

    render() {
        const {container, content, title, section, pickerWrapper, pickkerText, picker} = styles
        const {emailError, passwordError} = this.state

        const {email, password} = this.props.auth
        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>

                <View>
                    <TransparentHeader
                        arrowColor={'black'}
                        backButton
                        onPress={() => {
                            this.props.navigation.goBack(null)
                        }}
                        title={''}
                    />
                </View>

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={false}
                            onRefresh={null}
                        />
                    }>
                    <View style={content}>
                        <Text style={titleStyle}>Entrar</Text>
                        <View style={{flex: 1, marginTop: 60,}}>


                            <View style={{flexDirection: 'row', width: '100%', marginTop: 20}}>
                                <InputLogin
                                    icon={"email"}
                                    label={'Email'}
                                    autoCapitalize={'none'}
                                    error={emailError}
                                    onSubmitEditing={() => {
                                        this.passwordRef.focus();
                                    }}
                                    keyboardType={"email-address"}
                                    placeholder={'seuemail@gmail.com'}
                                    value={email}
                                    onChangeText={value => {
                                        this.setState({emailError: ''})
                                        this.props.authUpdate({prop: 'email', value: value.trim()})}}
                                />
                            </View>

                            <View style={{flexDirection: 'row', width: '100%',}}>

                                <InputLogin
                                    icon={"lock"}
                                    label={'Senha'}
                                    setRef={(input) => this.passwordRef = input}
                                    onSubmitEditing={() => { Keyboard.dismiss() }}
                                    autoCapitalize={'none'}
                                    secureTextEntry
                                    error={passwordError}

                                    value={password}
                                    placeholder={'Digite sua senha'}
                                    onChangeText={value => {
                                        this.setState({passwordError: ''})
                                        this.props.authUpdate({prop: 'password', value})}}

                                />
                            </View>

                            <View style={{width: '100%', alignItems: 'center', marginTop: 20}}>
                                <Text style={{color: 'red', fontWeight: '700'}}>{this.props.errorMessage}</Text>
                            </View>



                            <View style={{marginTop: 40}}>
                                <Button
                                    spinner
                                    disabled={this.props.loading}
                                    onPress={this.onLoginPress}
                                    label={this.props.loading? 'Entrando Aguarde...': 'Entrar'}/>
                            </View>



                            <View style={{width: '100%', alignItems: 'center', marginTop: 20}}>
                                <TouchableOpacity  style={seeMoreContainer}
                                                   onPress={()=>  this.props.navigation.navigate('passwordRecover',
                                                       {
                                                           action: PASSWORD_RECOVER,
                                                           title: "Recuperar Senha"
                                                       }
                                                       )}
                                                   >
                                <Text style={messageStyle}>Esqueceu sua senha? </Text>
                                </TouchableOpacity>
                            </View>


                        </View>
                    </View>
                </ScrollView>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: purpleTitle
    },
    content: {
        flex: 1,
        paddingHorizontal: 40
    },
    title: {
        fontWeight: '700', fontSize: 16,
    },

    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}
});


const mapStateToProps = (state) => {

    return {
        auth: state.auth.auth,
        loading: state.auth.loading,
        logged: state.auth.logged,
        errorMessage: state.auth.errorMessage,
        loginExpired: state.auth.loginExpired,

    }
}


export default connect(mapStateToProps, {
    loginServer,
    facebookLogin,
    authUpdate
})(LoginScreen)



