import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl,
    TouchableOpacity,
    Keyboard
} from 'react-native';
import {Container, Content,} from 'native-base';
import {connect} from 'react-redux'
import {authUpdate, signupServer, facebookLogin} from "../../../redux/actions";
import {Input, InputLogin, LoadingEvents, TransparentHeader, Button} from "../../common";
import {darkGrey, facebookBlue, purpleTitle, titleGrey} from "../../common/colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {titleStyle} from "../../common/styles/loginSignupStyle";
import {tag, tagWrapper} from "../../common/styles/geralStyle";
import {Picker} from "../../common/Picker";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'



const {height, width} = Dimensions.get('window')

class SignupScreen extends React.Component {
    static navigationOptions = {header: null}



    constructor(props) {
        super(props)
        this.state = {
            nameError: false,
            lastNameError: '',
            emailError: '',
            passwordError: '',
            confirmPasswordError: ''
        };
    }

    onPresssButton = () => {
        if (!this.checkError()) {
            this.props.signupServer(this.props.auth, this.props.errors);
        }
    }


    componentDidUpdate() {
        if (this.props.logged) {
            this.props.navigation.goBack(null)
        }
    }

    checkError() {
        const {name, lastName, email,  password, confirmPassword} = this.props.auth || {}
        let errors = false;
        if (!name) {
            this.setState({nameError: 'Nome obrigátorio'})
            errors = true
        }
        if (!lastName) {
            this.setState({lastNameError: 'Sobrenome obrigátorio'})
            errors = true
        }
        if (!email || !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            this.setState({emailError: 'Email incompleto'})
            errors = true
        }

        if (!password) {
            this.setState({passwordError: 'Senha obrigátoria'})
            errors = true
        }
        if (password && password.length < 8 || password.length > 14) {
            this.setState({passwordError: 'Senha deve ter entre 8 e 14 caracteres'})
            errors = true
        }
        if (!confirmPassword) {
            this.setState({confirmPasswordError: 'Confirmação de senha obrigátoria'})
            errors = true
        }

        if (password && confirmPassword && confirmPassword !== password) {
            this.setState({confirmPasswordError: 'Senha e Confirme sua senha Não conferem'})
            errors = true
        }


        return errors

    }

    render() {
        const {container, content, title, section, pickerWrapper, pickkerText, picker} = styles
        const {nameError, lastNameError, emailError, passwordError, confirmPasswordError, } = this.state

        const {
            name, lastName, email, password, confirmPassword,
            gender, acceptTherms,
        } = this.props.auth || {}

        const {errors, loadingSignup, errorSingupMessage} = this.props
        return (
            <View style={{flex: 1, backgroundColor: "white"}}>

                <View>
                    <TransparentHeader
                        arrowColor={'black'}
                        backButton
                        onPress={() => {
                            this.props.navigation.goBack(null)
                        }}
                        title={''}
                    />

                </View>

                <KeyboardAwareScrollView>

                    <View style={content}>

                        <Text style={titleStyle}>Criar Conta</Text>


                        <View style={{flex: 1, marginTop: 30}}>


                            <View style={{flexDirection: 'row', width: '100%', marginTop: 20,}}>


                                <InputLogin

                                    icon={"account"}
                                    label={'Nome'}
                                    error={nameError}
                                    placeholder={'nome'}
                                    value={name}
                                    setRef={(input) => this.nameRef = input}
                                    onSubmitEditing={() => {
                                        this.lastNameRef.focus();
                                    }}

                                    onChangeText={value => {
                                        this.setState({nameError: ''})
                                        this.props.authUpdate({prop: 'name', value})
                                    }
                                    }

                                />

                                <View style={{width: 5}}></View>


                                <InputLogin
                                    icon={"account"}
                                    label={'Sobrenome'}
                                    error={lastNameError}
                                    placeholder={'sobrenome'}
                                    value={lastName}
                                    setRef={(input) => this.lastNameRef = input}
                                    onSubmitEditing={() => {
                                        this.emailRef.focus();
                                    }}
                                    onChangeText={value => {
                                        this.setState({lastNameError: ''})
                                        this.props.authUpdate({prop: 'lastName', value})
                                    }
                                    }

                                />
                            </View>


                            <View style={{flexDirection: 'row', width: '100%',}}>
                                <InputLogin
                                    icon={"email"}
                                    autoCapitalize={'none'}
                                    error={emailError}
                                    label={'Email'}
                                    keyboardType={"email-address"}
                                    value={email}
                                    setRef={(input) => this.emailRef = input}
                                    onSubmitEditing={() => {
                                        this.passwordRef.focus();
                                    }}
                                    placeholder={'seuemail@gmail.com'}
                                    onChangeText={value => {
                                        this.setState({emailError: ''})
                                        this.props.authUpdate({prop: 'email', value: value.trim()})
                                    }}

                                />
                            </View>


                            {/*Senha*/}
                            <View style={{flexDirection: 'row', width: '100%',}}>
                                <InputLogin
                                    icon={"lock"}
                                    label={'Senha'}
                                    error={passwordError}
                                    secureTextEntry
                                    value={password}
                                    setRef={(input) => this.passwordRef = input}
                                    onSubmitEditing={() => {
                                        this.repeatPassworfRef.focus();
                                    }}
                                    placeholder={'digite sua senha'}
                                    onChangeText={value => {
                                        this.setState({passwordError: ''})
                                        this.props.authUpdate({prop: 'password', value})
                                    }}

                                />
                            </View>

                            {/*Confirmar Senha*/}
                            <View style={{flexDirection: 'row', width: '100%',}}>
                                <InputLogin
                                    icon={"lock"}
                                    label={'Confirme a senha'}
                                    error={confirmPasswordError}
                                    secureTextEntry
                                    value={confirmPassword}
                                    setRef={(input) => this.repeatPassworfRef = input}
                                    onSubmitEditing={() => { Keyboard.dismiss() }}
                                    placeholder={'repita a sua senha'}
                                    onChangeText={value => {
                                        this.setState({confirmPasswordError: ''})
                                        this.props.authUpdate({
                                            prop: 'confirmPassword',
                                            value
                                        }, password)
                                    }
                                    }


                                />
                            </View>


                            <View style={{width: '100%', alignItems: 'center', marginTop: 20}}>
                                <Text style={{color: 'red', fontWeight: '700'}}>{errorSingupMessage}</Text>
                            </View>


                            <View style={{marginTop: 40, marginBottom: 40}}>
                                <Button
                                    spinner
                                    disabled={loadingSignup}
                                    onPress={this.onPresssButton}
                                    label={loadingSignup ? 'Criando Aguarde...' : 'Criar Conta'}/>
                            </View>


                        </View>
                    </View>

                </KeyboardAwareScrollView>


            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: purpleTitle
    },
    content: {
        flex: 1,
        paddingHorizontal: 30,
    },
    title: {
        fontWeight: '700', fontSize: 16,
    },
    section: {marginTop: 20},
    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}
});


const mapStateToProps = (state) => {

    return {
        auth: state.auth.auth,
        logged: state.auth.logged,
        loadingSignup: state.auth.loadingSignup,
        errorSingupMessage: state.auth.errorSingupMessage,

    }
}


export default connect(mapStateToProps, {
    signupServer,
    authUpdate,
    facebookLogin
})(SignupScreen)

