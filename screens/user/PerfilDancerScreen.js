import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableHighlight,
    Keyboard,
    ScrollView,
    ActivityIndicator,
    TouchableOpacity,
    TextInput,
    Alert
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {ImagePicker, Permissions, IntentLauncherAndroid} from 'expo';

import {perfilImageUpdate, perfilImageUpdateServer, fetch_styles, dancerInfoUpdateAction} from "../../redux/actions";
import {connect} from "react-redux";

import _ from 'lodash'
import {Header} from "../../components/Header";
import {
    buttonWrapperEditProfile,
    errorMessageContainer, errorMessageStyle, imageContainer, imagePickerContainer,
    nameLastNameContainer
} from "../../components/common/styles/perfilStyle";
import {grey, logoBlue, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import {Input} from "../../components/common/Input";
import {InputMask} from "../../components/common/InputMask";
import {Button} from "../../components/common/Button";
import {Avatar, Card, Title, Paragraph} from 'react-native-paper';
import {LevelPicker} from "../../components/common/LevelPicker";
import {StylesPicker} from "../../components/StylesPicker";
import {perfilTitleStyle} from "../../components/common/styles/geralStyle";

class PerfilDancerScreen extends Component {
    static navigationOptions = {header: null};

    constructor(props) {
        super(props);
        this.state = {};
        this.props.fetch_styles()
    }


    pickImage = async (type) => {
        let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('Você deve liberar as permissões para acessar a camera');
        } else {
            let result = await ImagePicker.launchImageLibraryAsync({
                base64: true,
                allowsEditing: true,
                aspect: [4, 3],
            });
            if (!result.cancelled) {
                this.setState({imageChange: true})
                const value = result.uri
                this.props.perfilImageUpdate({prop: type, value})
                //  this.props.perfilImageUpdateServer(this.props.user, result, type)

                /**/
            } else {
                console.log('aqui')
            }
        }
    }

    styleUpdate = (style) =>{
        const {selectedStyles} = this.props.perfilDancer || {}
        const found = _.find(selectedStyles,style);
        if(found){
            _.remove(selectedStyles, found);
        }else{
            selectedStyles.push(style)
        }
        this.props.dancerInfoUpdateAction({prop: 'selectedStyles', value: selectedStyles})

    }


    pressButton = () => {
        const {navigation} = this.props || {}
        const {firstTime} = navigation.state.params || {};
        firstTime ? navigation.navigate('welcome') : navigation.goBack(null)
    }

    render() {


        const {navigation, perfilDancer, styles: estilos, dancerInfoUpdateAction} = this.props || {}

        const {image1, image2, image3, image4, image5, image6, level, description, selectedStyles} = perfilDancer || {}
        const {url: url1} = image1 || {}
        const {url: url2} = image2 || {}
        const {url: url3} = image3 || {}
        const {url: url4} = image4 || {}
        const {url: url5} = image5 || {}
        const {url: url6} = image6 || {}

        const {content, container} = styles;
        return (
            <View style={container}>
                <View>

                    <Header
                        backButton
                        onPress={() => {
                            navigation.goBack(null)
                        }}
                        title={"Perfil Dancer"}
                    />
                </View>
                <KeyboardAwareScrollView>
                    <ScrollView>
                        <View>
                            <View style={content}>

                                <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>

                                    <View style={{borderRadius: 20, overflow: 'hidden', width: '33%', height: 150}}>
                                        <TouchableOpacity
                                            style={{flex: 1}}
                                            onPress={() => this.pickImage('image1')}>
                                            <Image
                                                style={{flex: 1}}
                                                resizeMode={'cover'}
                                                source={{uri: url1}}/>


                                        </TouchableOpacity>
                                    </View>


                                    <View style={{borderRadius: 20, overflow: 'hidden', width: '33%', height: 150}}>
                                        <TouchableOpacity
                                            style={{flex: 1}}
                                            onPress={() => this.pickImage('image2')}>
                                            <Image
                                                style={{flex: 1}}
                                                resizeMode={'cover'}
                                                source={{uri: url2}}/>


                                        </TouchableOpacity>
                                    </View>

                                    <View style={{borderRadius: 20, overflow: 'hidden', width: '33%', height: 150}}>
                                        <TouchableOpacity
                                            style={{flex: 1}}
                                            onPress={() => this.pickImage('image3')}>
                                            <Image
                                                style={{flex: 1}}
                                                resizeMode={'cover'}
                                                source={{uri: url3}}/>


                                        </TouchableOpacity>
                                    </View>
                                </View>


                                <View style={{marginTop: 25}}>
                                    <LevelPicker
                                        levelUpdate={(value) => {
                                            this.props.dancerInfoUpdateAction({
                                                prop: "level",
                                                value: value
                                            })
                                        }}
                                        level={level} />
                                </View>

                                <View style={{marginTop: 5, width: '100%'}}>
                                    <StylesPicker
                                        selectedStyles={selectedStyles}
                                        styleUpdate={this.styleUpdate}
                                        estilos={estilos}/>
                                </View>


                                <View style={{marginTop: 5, width: '100%'}}>
                                    <Text style={perfilTitleStyle}>{'Sobre Mim'}</Text>
                                    <View style={{
                                        borderColor: 'grey',
                                        borderWidth: 1,
                                        borderRadius: 5,
                                        padding: 5
                                    }}>
                                        <TextInput
                                            style={{
                                                height: 100,
                                                justifyContent: "flex-start", fontWeight: '700', fontSize: 16,
                                                color: 'white',
                                            }}
                                            multiline={true}
                                            numberOfLines={4}
                                            onChangeText={(value) =>  dancerInfoUpdateAction({prop: 'description', value})}
                                            value={description}/>
                                    </View>

                                </View>


                                <View style={{marginTop: 20, marginBottom: 40, alignItems: 'center'}}>
                                    <Button
                                        onPress={this.pressButton}
                                        label={'   SALVAR   '}/>
                                </View>


                            </View>
                        </View>


                    </ScrollView>
                </KeyboardAwareScrollView>


            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1F1E26'
    },
    content: {
        flex: 1,
        paddingHorizontal: 20,
    },
    title: {
        fontWeight: '700', fontSize: 16,
    },
    inputWrapper: {
        margin: 3,
        width: '100%',

        borderBottomWidth: 0.5,
        borderColor: purpleTitle,
        padding: 3,

    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',
    },
    section: {marginTop: 20},
    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}

});

const mapStateToProps = (state) => {

    return {
        perfilDancer: state.dancer.perfilDancer,
        styles: state.adm.styles,
    }
};


export default connect(mapStateToProps, {perfilImageUpdate, perfilImageUpdateServer, fetch_styles, dancerInfoUpdateAction})(PerfilDancerScreen)


