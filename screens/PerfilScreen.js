import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

import {connect} from "react-redux";
import {Header} from "../components/Header";
import {logoutAction, perfilImageUpdate, perfilImageUpdateServer, bannerImageUpdate} from "../redux/actions";

import {imgAvatar, txtEmail, txtName, userInfoWrapper} from "../components/common/styles/perfilStyle";
import {purpleTitle, titleColor} from "../components/common/colors/index";
import {ImagePicker, LinearGradient, Permissions} from 'expo'
import {StackActions, NavigationActions} from 'react-navigation';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';
import {PerfilButton} from "../components/common/PerfilButton";


class PerfilScreen extends React.Component {

    static navigationOptions = {header: null}

    constructor(props) {
        super(props)
        this.state = {};


        //  this.props.fetchEvents(this.state.streetEvent, 0, 10);
    }


    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):

    }

    pickImage = async (type) => {
        let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('Você deve liberar as permissões para acessar a camera');
        } else {
            let result = await ImagePicker.launchImageLibraryAsync({
                base64: true,
                allowsEditing: true,
                aspect: [4, 3],
            });
            if (!result.cancelled) {
                this.setState({imageChange: true})
                const value = result.uri
                this.props.perfilImageUpdate({prop: 'url', value}, type)
                this.props.perfilImageUpdateServer(this.props.user, result, type)

                /**/
            } else {
                console.log('aqui')
            }
        }
    }

    activeTicketNavigation = () => {
        const {user} = this.props
        console.log(user)

        if (user.has('establishmentId')) {
            this.props.navigation.navigate('ActiveTicket')
        } else {
            Alert.alert("", 'Voce deve Editar o estabelecimento antes de cadastrar um ticket ativo');
            this.props.navigation.navigate('EditEstablishment')
        }
    }

    callbackLogout = () => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'login'})],
        });
        this.props.navigation.dispatch(resetAction);
    }


    renderView = () => {
        const {logged, user, userJson, navigation} = this.props

        const {userImage, corporateName, name, banner, email, type} = userJson || {}
        const {url} = userImage || {}

        console.log(type)


        return (
            <ScrollView

                scrollEventThrottle={16}>
                <View style={{flex: 1}}>

                    <View style={{paddingHorizontal: 20,}}>


                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <TouchableOpacity onPress={() => this.pickImage('userImage')}>
                                <Image
                                    style={{width: 80, height: 80, borderRadius: 40}}
                                    source={{uri: url || 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxN78sbQzaZ3mHw2fVuek-2eEtBZbBx5C2oHCrFHU77CYj67kV'}}/>
                            </TouchableOpacity>

                            <View style={{marginLeft: 10}}>
                                <Text style={{fontSize: 18, fontWeight: '700', color: 'grey'}}>{name}</Text>
                                <Text style={{fontSize: 18, fontWeight: '700', color: 'grey'}}>{email}</Text>
                            </View>

                        </View>


                        <View>
                            <View style={{marginTop: 20}}><Text
                                style={{fontSize: 18, fontWeight: '700', color: titleColor}}>Informações</Text>
                            </View>
                            <View style={{marginTop: 5, paddingHorizontal: 10}}>
                                <PerfilButton
                                    label={'Perfil Dançarino'}
                                    onPress={() => navigation.navigate('perfilDancer')}/>



                            </View>
                        </View>


                        <View>
                            <View style={{marginTop: 20}}><Text
                                style={{fontSize: 18, fontWeight: '700', color: 'white'}}>Legal</Text>
                            </View>
                            <View style={{marginTop: 5, paddingHorizontal: 10}}>
                                <PerfilButton
                                    label={'Termos de Compromisso'}
                                    onPress={() => navigation.navigate('therms', {title: 'Termos de Compromisso'})}/>
                                <PerfilButton
                                    label={'Política de Privacidade'}
                                    onPress={() => navigation.navigate('therms', {title: 'Política de Privacidade '})}/>


                            </View>
                        </View>


                        <View style={{marginTop: 10}}><Text
                            style={{fontSize: 18, fontWeight: '700', color: 'white'}}>Outros</Text></View>
                        <View style={{marginTop: 5, paddingHorizontal: 10, marginBottom: 20}}>

                            <PerfilButton
                                label={'Encerrar Sessão'}
                                onPress={() => this.props.logoutAction(this.callbackLogout)}/>


                        </View>
                    </View>


                </View>


            </ScrollView>
        )


    }

    render() {
        const {logged, userJson} = this.props
        const {type} = userJson || {}
        console.log(type)

        return (


            <View style={styles.container}>
                <Header
                    colorText={titleColor}
                    title={'Perfil'}
                />

                {this.renderView()}


            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1F1E26',


    },
    header: {
        marginTop: 20,
        height: this.startHeaderHeight,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
    },

});

const mapStateToProps = (state) => {


    return {
        logged: state.auth.logged,
        auth: state.auth.auth,
        user: state.user.user,
        userJson: state.user.userJson
    }
}


export default connect(mapStateToProps, {logoutAction, perfilImageUpdate, perfilImageUpdateServer})(PerfilScreen)

