import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

import {connect} from "react-redux";
import {fetch_events} from "../../redux/actions";


import {Header} from "../../components/Header";
import EventCardItem from "./EventCardItem";


class EventListScreen extends React.Component {

    static navigationOptions = {header: null}

    constructor(props) {
        super(props)
        this.state = {};


        this.props.fetch_events();
          
    }


    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):

    }

    renderItem = (item, index) => {



        return (
            <View style={{marginRight:10}}>
                <EventCardItem event={item}/>

            </View>
        )
    }






    render() {
        const {logged, events} = this.props

        return (
            <View style={styles.container}>
                <FlatList
                    refreshing={false}
                    horizontal
                    onRefresh={() => console.log('refreshing')}
                    keyExtractor={(item, index) => item.objectId}
                    showsVerticalScrollIndicator={false}
                    style={{flex: 1,}}
                    data={events}
                    onEndReached={this.handleLoadMore}
                    onEndThreshold={0}
                    renderItem={({item, index}) => this.renderItem(item, index)}


                />



            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1F1E26',
    },


});

const mapStateToProps = (state) => {
    return {
        events: state.event.events
    }
};


export default connect(mapStateToProps, {fetch_events})(EventListScreen)

