import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Dimensions, Image, Alert, TouchableWithoutFeedback} from 'react-native';
import {grey, purpleTitle, titleColor} from "../../components/common/colors/index";
import {tagWrapper, tag} from "../../components/common/styles/geralStyle";
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';
import { LinearGradient } from 'expo-linear-gradient';

import {connect} from "react-redux";


class EventCardItem extends React.Component {
    state = {
        loading: false,
        visible: false,
        userImageVisible: false,
        postImageVisible: false,
    };




    render() {

        const {loading} = this.state
        const {event, width, height} = this.props


        console.log(event)


        return (

            < TouchableWithoutFeedback
                style={{padding: 10, marginTop: 50,borderRadius: 20,marginRight: 10}}

                onPress={this.props.onPress}>
                <Card style={{elevation: 11, width: 250,height: 150,  }}>

                    <View style={{flex: 1}}>

                        <Card.Cover source={{uri: 'https://images.sympla.com.br/5c320291116fe.png'}} style={{ flex: 1}}>

                        </Card.Cover>


                        <View style={{
                            position: 'absolute',
                            width: '100%',
                            bottom: 0,
                        }}>
                            <LinearGradient
                                colors={['#00000000', '#00000095',]}
                                style={{flex: 1, padding: 10, flexDirection: 'row',}}>

                                <View style={{flex: 2,  justifyContent: 'flex-end',}}>
                                    <Text
                                        style={{color: 'white', fontWeight: '700', fontSize: 22,  textAlign: 'right'}}
                                    >{'Evento Nome'}</Text>

                                </View>
                            </LinearGradient>


                        </View>
                    </View>

                    {/*  <Card.Actions>
                    <Button
                        color={loading ? grey : null}
                        onPress={loading ? null : this.submitTicket}>{loading ? 'Salvando, aguarde' : 'Salvar'}</Button>
                </Card.Actions>*/}
                </Card>
            </TouchableWithoutFeedback>

        )
    }


}

const styles = StyleSheet.create({});



export default EventCardItem
