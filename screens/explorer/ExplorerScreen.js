import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

import {connect} from "react-redux";
import {fetch_events} from "../../redux/actions";
import EventListScreen from '../events/EventListScreen'

import {Header} from "../../components/Header";


class ExplorerScreen extends React.Component {

    static navigationOptions = {header: null}

    constructor(props) {
        super(props)
        this.state = {};




    }


    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):

    }






    render() {
        const {logged, categories} = this.props

        return (


            <View style={styles.container}>
                <Header
                    title={"Step In"}
                />

                <View style={{flex: 1,padding:20}}>
                    <EventListScreen/>
                </View>







            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1F1E26',



    },


});

const mapStateToProps = (state) => {

    return {

    }
};


export default connect(mapStateToProps, {fetch_events})(ExplorerScreen)

