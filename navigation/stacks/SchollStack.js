import React from 'react';
import { createStackNavigator} from 'react-navigation';

import {purpleTitle} from "../../components/common/colors/index";

import PerfilScreen from "../../screens/PerfilScreen";
import PerfilDancerScreen from "../../screens/user/PerfilDancerScreen";
import TabBarIcon from "../../components/TabBarIcon";






const  SchollStack =  createStackNavigator({
    perfil: PerfilScreen,
    perfilDancer: PerfilDancerScreen,


},  {
    tabBarOptions: {
        header: null,
        activeTintColor:purpleTitle,
        inactiveTintColor: 'grey',
        style: {
            backgroundColor: 'white',
            borderTopWidth: 0,
            shadowOffset: {width: 5, height: 3},
            shadowColor: 'black',
            shadowOpacity: 0.5,
            elevation: 5

        }
    }
});



SchollStack.navigationOptions =({navigation}) =>{

    let tabBarVisible = true;
    if(navigation.state.index >0){
        tabBarVisible = false;
    }

    return {
        header: null,

        tabBarVisible,
        tabBarLabel: 'Escolas',
        tabBarIcon: ({ focused }) => (


            <TabBarIcon
                focused={focused}
                name={'home'}
            />
        ),
    };
};




export default SchollStack
