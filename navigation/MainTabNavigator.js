import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/PerfilScreen';
import PerfilScreen from "../screens/PerfilScreen";
import ExplorerScreen from "../screens/explorer/ExplorerScreen";
import {purpleTitle} from "../components/common/colors/index";
import IntroScreen from "../screens/auth/IntroScreen";
import PerfilDancerScreen from "../screens/user/PerfilDancerScreen";
import PerfilStack from '../navigation/stacks/PerfilStack'
import SchollStack from '../navigation/stacks/SchollStack'

const config = Platform.select({
    web: {headerMode: 'screen'},
    default: {},
});

const HomeStack = createStackNavigator(
    {
        explorer: ExplorerScreen,

    },
    config
);

HomeStack.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={
                'magnify'
            }
        />
    ),
};

HomeStack.path = '';

const LinksStack = createStackNavigator(
    {
        Links: LinksScreen,
    },
    config
);

LinksStack.navigationOptions = {
    tabBarLabel: 'Profissionais',
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={'school'}/>
    ),
};

LinksStack.path = '';



const tabNavigator = createBottomTabNavigator({
    HomeStack,
    SchollStack,
    LinksStack,
    PerfilStack,
}, {
    tabBarOptions: {
        header: null,
        activeTintColor: purpleTitle,
        inactiveTintColor: 'grey',
        style: {
            backgroundColor: '#1e2526',
            borderTopWidth: 0,
            shadowOffset: {width: 5, height: 3},
            shadowColor: 'black',
            shadowOpacity: 0.5,
            elevation: 5

        }
    }
});

tabNavigator.navigationOptions = {
    header: null,

};

tabNavigator.path = '';


const mainStack = createStackNavigator(
    {
        intro: IntroScreen,
        main : tabNavigator
    },
    config
);


export default mainStack;
